<?php

namespace App\Http\Controllers;

use App\Models\People;
use App\Models\Planet;
use App\Models\Species;
use Illuminate\Support\Facades\Http;

class PeopleController extends Controller
{
    public function index($id)
    {
        $response = Http::get('https://swapi.dev/api/people/' . $id);
        $peopleData = json_decode($response->body(), true);
        if (isset($peopleData['detail'])) {
            return response()->json($peopleData['detail'], 500);
        }

        $people = People::find($id);
        if ($people) {
            return response()->json($people, 200);
        }

        $peopleData = array_merge(['id' => $id], $peopleData);
        /** @var People $people */
        $people = People::create($peopleData);

        if (!empty($peopleData['species'])) {
            $speciesId = (int) filter_var($peopleData['species'][0], FILTER_SANITIZE_NUMBER_INT);
            $species = Species::find($speciesId);
            if (!$species) {
                $species = Species::create(['id' => $speciesId]);
            }
            $people->species()->associate($species)->save();
        }

        if (!empty($peopleData['homeworld'])) {
            $planetId = (int) filter_var($peopleData['homeworld'], FILTER_SANITIZE_NUMBER_INT);
            $planet = Planet::find($planetId);
            if (!$planet) {
                $planet = Planet::create(['id' => $planetId]);
            }
            $people->planets()->associate($planet)->save();
        }

        return response()->json($people, 201);
    }
}
