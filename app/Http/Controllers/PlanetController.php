<?php

namespace App\Http\Controllers;

use App\Models\Planet;
use Illuminate\Support\Facades\Http;

class PlanetController extends Controller
{
    public function index($id)
    {
        $response = Http::get('https://swapi.dev/api/planets/' . $id);
        $planetData = json_decode($response->body(), true);
        if (isset($planetData['detail'])) {
            return response()->json($planetData['detail'], 500);
        }

        $planet = Planet::find($id);
        if ($planet) {
            $planet->update($planetData);
            return response()->json($planet, 200);
        }

        $planetData = array_merge(['id' => $id], $planetData);
        $planet = Planet::create($planetData);
        return response()->json($planet, 201);
    }
}
