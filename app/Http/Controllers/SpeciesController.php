<?php

namespace App\Http\Controllers;

use App\Models\Species;
use Illuminate\Support\Facades\Http;

class SpeciesController extends Controller
{
    public function index($id)
    {
        $response = Http::get('https://swapi.dev/api/species/' . $id);
        $speciesData = json_decode($response->body(), true);
        if (isset($speciesData['detail'])) {
            return response()->json($speciesData['detail'], 500);
        }

        $species = Species::find($id);
        if ($species) {
            $species->update($speciesData);
            return response()->json($species, 200);
        }

        $speciesData = array_merge(['id' => $id], $speciesData);
        $species = Species::create($speciesData);
        return response()->json($species, 201);
    }
}
