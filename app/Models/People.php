<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class People extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'birth_year',
        'eye_color',
        'gender',
        'hair_color',
        'height',
        'mass',
        'skin_color',
        'films',
        'starships',
        'vehicles',
        'url',
        'created',
        'edited',
    ];

    protected $casts = [
        'films' => 'array',
        'starships' => 'array',
        'vehicles' => 'array',
    ];

    public $incrementing = false;

    protected $primaryKey = 'id';

    public function species(): BelongsTo
    {
        return $this->belongsTo(Species::class);
    }

    public function planets(): BelongsTo
    {
        return $this->belongsTo(Planet::class);
    }
}
