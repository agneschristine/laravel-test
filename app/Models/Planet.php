<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Planet extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'diameter',
        'rotation_period',
        'orbital_period',
        'gravity',
        'population',
        'climate',
        'terrain',
        'surface_water',
        'films',
        'url',
        'created',
        'edited',
    ];

    protected $casts = [
        'films' => 'array',
    ];

    public $incrementing = false;

    protected $primaryKey = 'id';

    public function residents(): HasMany
    {
        return $this->hasMany(People::class);
    }
}
