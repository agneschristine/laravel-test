<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Species extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'classification',
        'designation',
        'average_height',
        'average_lifespan',
        'eye_colors',
        'hair_colors',
        'skin_colors',
        'language',
        'homeworld',
        'films',
        'url',
        'created',
        'edited',
    ];

    protected $casts = [
        'films' => 'array',
    ];

    public $incrementing = false;

    protected $primaryKey = 'id';

    public function people(): HasMany
    {
        return $this->hasMany(People::class);
    }
}
