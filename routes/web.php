<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/people/{id}', [\App\Http\Controllers\PeopleController::class, 'index']);

Route::get('/species/{id}', [\App\Http\Controllers\SpeciesController::class, 'index']);

Route::get('/planet/{id}', [\App\Http\Controllers\PlanetController::class, 'index']);
